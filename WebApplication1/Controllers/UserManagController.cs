﻿using System;
using WebApplication1.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WebApplication1.Controllers
{
    public class UserManagController : Controller
    {
        // GET: UserManag
        private readonly IConfiguration configuration;
        public UserManagController(IConfiguration config)
        {
            this.configuration = config;
        }
        public ActionResult reader_check()
        {
            Update Update = new Update();
            string connectionString = configuration.GetConnectionString("connectionstring");
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            SqlCommand com = new SqlCommand("SELECT [read_name],[read_id_card],[read_add_date],[read_remark] FROM[Reader].[dbo].[readsgin]", connection);
            SqlDataReader dr = com.ExecuteReader();

            string reader_name = "";
            string reader_idnum = "";
            string appdate = "";
            string remark = "";

            while (dr.Read())
            {
                reader_name = dr["read_name"].ToString();
                reader_idnum = dr["read_id_card"].ToString();
                appdate = dr["read_add_date"].ToString();
                remark = dr["read_remark"].ToString();
            }
            ViewBag.reader_name = reader_name;
            ViewBag.reader_idnum = reader_idnum;
            ViewBag.remark = remark;
            ViewBag.appdate = appdate;
            ViewBag.Update = Update;
            return View();
        }
        [HttpPost]
        public ActionResult test(IFormCollection post)
        {
            string keyword = post["keyword"];
            Update data = new Update(keyword);
            return View(data);
        }
        public ActionResult reader_manage()
        {
            return View();
        }
        // GET: UserManag/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult reader_AppList()
        {
            return View();
        }
        public ActionResult list_over_due()
        {
            return View();
        }
    }
}