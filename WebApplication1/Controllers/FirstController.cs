﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers
{
    public class FirstController : Controller
    {
        // GET: /<controller>/

        private readonly IConfiguration configuration;


        public FirstController(IConfiguration config)
        {
            this.configuration = config;
        }
        public IActionResult Index()
        {
            string connectionString = configuration.GetConnectionString("connectionstring");
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            SqlCommand com = new SqlCommand("SELECT   [sch_tel],[sch_email] FROM[Admini].[dbo].[School]", connection);
            //  var count = (int)com.ExecuteScalar();
            SqlDataReader dr = com.ExecuteReader();
            //string adm = "";
            //string schoolname = "";
            string email = "";
            string tel = "";
            while (dr.Read()) {
              //schoolname = dr["sch_name"].ToString();
                //adm = dr = [""]
                tel = dr["sch_tel"].ToString();
                email = dr["sch_email"].ToString();
            }
            ViewBag.tel = tel;
            ViewBag.email = email;
            //ViewData["cnt"] = count;
            connection.Close();
            return View();
        }


    }
}
