﻿using System;
using WebApplication1.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;


namespace WebApplication1.Controllers
{
    public class LibraryController : Controller
    {
        // GET: Library
        private readonly IConfiguration configuration;
        public LibraryController(IConfiguration config)
        {
            this.configuration = config;
        }
        public ActionResult MemberData()
        {
            string connectionString = configuration.GetConnectionString("connectionstring");
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            SqlCommand com = new SqlCommand("SELECT * FROM[Admini].[dbo].[School]", connection);
            //  var count = (int)com.ExecuteScalar();
            SqlDataReader dr = com.ExecuteReader();
            string name = "";
            string person = "";
            string tel = "";
            string url = "";
            string lim = "";
            string Open = "";
            string satOpen = "";
            string sunOpen = "";
            string sucmail = "";
            string failmail = "";
            string firsucmail = "";
            string mark = "";
            while (dr.Read())
            {
                name = dr["sch_name"].ToString();
                person = dr["sch_person"].ToString();
                tel = dr["sch_tel"].ToString();
                url = dr["sch_url"].ToString();
                lim = dr["sch_lim_num"].ToString();
                Open = dr["sch_usual_open"].ToString();
                satOpen = dr["sch_sat_open"].ToString();
                sunOpen = dr["sch_sun_open"].ToString();
                sucmail = dr["sch_success_mail_newly"].ToString();
                failmail = dr["sch_fail_mail"].ToString();
                firsucmail = dr["sch_success_mail_first"].ToString();
                mark = dr["sch_remark"].ToString();
            }
            ViewBag.name = name;
            ViewBag.person = person;
            ViewBag.tel = tel;
            ViewBag.url = url;
            ViewBag.lim = lim;
            ViewBag.Open = Open;
            ViewBag.satOpen = satOpen;
            ViewBag.sunOpen = sunOpen;
            ViewBag.sucmail = sucmail;
            ViewBag.failmail = failmail;
            ViewBag.firsucmail = firsucmail;
            ViewBag.mark = mark;
            // ViewData["cnt"] = count;

            //------------------------進modle撈資料--------------------------------

            SelectModel data = new SelectModel();
            ViewBag.MemberData = data;
            List<SelectModel> list = new List<SelectModel>();
            //list.Add(new SelectModel(name, person, tel, url, lim, Open, satOpen, sunOpen, sucmail, failmail, firsucmail, mark));
            ViewBag.List = list;
            connection.Close();
            return View(data);
        }
        public ActionResult SchoolLim()
        {
            string connectionString = configuration.GetConnectionString("connectionstring");
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            SqlCommand com = new SqlCommand("SELECT [sch_name],[sch_lim_num] FROM[Admini].[dbo].[School]", connection);
            SqlDataReader dr = com.ExecuteReader();

            string name = "";
            string lim_num = "";

            while (dr.Read())
            {
                name = dr["sch_name"].ToString();
                lim_num = dr["sch_lim_num"].ToString();
            }
            ViewBag.name = name;
            ViewBag.lim_num = lim_num;

            return View();
        }
       
    }
}