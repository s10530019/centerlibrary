﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WebApplication1.Controllers
{
    public class AdManagController : Controller
    {
        // GET: AdManag
        private readonly IConfiguration configuration;
        public AdManagController(IConfiguration config)
        {
            this.configuration = config;
        }
        public ActionResult Edituser()
        {
            string connectionString = configuration.GetConnectionString("connectionstring");
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            SqlCommand com = new SqlCommand("SELECT [adm_name],[adm_id],[adm_remark],[adm_level] FROM[Admini].[dbo].[AdminID]", connection);
            SqlDataReader dr = com.ExecuteReader();

            string adname = "";
            string ad_account = "";
            string remark = "";
            string level = "";

            while (dr.Read())
            {
                adname = dr["adm_name"].ToString();
                ad_account = dr["adm_id"].ToString();
                remark = dr["adm_remark"].ToString();
                level = dr["adm_level"].ToString();
            }
            ViewBag.adname = adname;
            ViewBag.ad_account = ad_account;
            ViewBag.remark = remark;
            ViewBag.level = level;

            return View();
        }
        //POST: AdManag
        [HttpPost]
        public ActionResult Updateuser(FormCollection post)
        {
            string status = post["adupdate"];
            if(status == "修改")
            {

            }
            return View();
        }
        // GET: AdManag/Details/5
       

        // GET: AdManag/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdManag/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AdManag/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AdManag/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AdManag/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AdManag/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}