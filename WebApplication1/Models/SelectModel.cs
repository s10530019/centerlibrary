﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace WebApplication1.Models
{
    public class SelectModel
    {
       /* public string ConString()
        {
            var builder = new ConfigurationBinder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var config = builder.Build();
            string constring = config.GetConnectionString("connectonstring");
            return constring;
        }*/
        
        // GET: Library
        /*private readonly IConfiguration configuration;

        string connectionString = configuration.GetConnectionString("connectionstring");
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
            欄位初始設定式無法參考非靜態欄位、方法或屬性 'SelectModel.configuration'*/ 


        List<SelectModel> detail = new List<SelectModel>();

        public string Sch_name { get; set; }
        public string Sch_person { get; set; }
        public string Sch_tel { get; set; }
        public string Sch_url { get; set; }
        public string Sch_lim_num { get; set; }
        public string Sch_usual_open { get; set; }
        public string Sch_sat_open { get; set; }
        public string Sch_sun_open { get; set; }
        public string Sch_success_mail_newly { get; set; }
        public string Sch_fail_mail { get; set; }
        public string Sch_success_mail_first { get; set; }
        public string Sch_remark { get; set; }
        
        //---------------先給變數一個預設值--------------------
        public SelectModel()
        {
            Sch_name = string.Empty;
            Sch_person = string.Empty;
            Sch_tel = string.Empty;
            Sch_url = string.Empty;
            Sch_lim_num = string.Empty;
            Sch_usual_open = string.Empty;
            Sch_sat_open = string.Empty;
            Sch_sun_open = string.Empty;
            Sch_success_mail_newly = string.Empty;
            Sch_fail_mail = string.Empty;
            Sch_success_mail_first = string.Empty;
            Sch_remark = string.Empty;
        }
        //------------將資料庫(?)的值帶入變數-----------------------------
        public SelectModel(string _sch_name, string _sch_person, string _sch_tel, string _sch_url, string _sch_lim_num, string _sch_usual_open, string _sch_sat_open, string _sch_sun_open, string _sch_success_mail_newly, string _sch_fail_mail, string _sch_success_mail_first, string _sch_remark)
        {
            Sch_name = _sch_name;
            Sch_person = _sch_person;
            Sch_tel = _sch_tel;
            Sch_url = _sch_url;
            Sch_lim_num = _sch_lim_num;
            Sch_usual_open = _sch_usual_open;
            Sch_sat_open = _sch_sat_open;
            Sch_sun_open = _sch_sun_open;
            Sch_success_mail_newly = _sch_success_mail_newly;
            Sch_fail_mail = _sch_fail_mail;
            Sch_success_mail_first = _sch_success_mail_first;
            Sch_remark = _sch_remark;
        }

        
    }
}
