﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Update
    {
        public string keyword { get; set; }
        public Update()
        {
            keyword = string.Empty;
        }
        public Update(string _keyword)
        {
            keyword = _keyword;
        }
        public override string ToString()
        {
            return $"關鍵字:{keyword}.";
        }
    }
}
